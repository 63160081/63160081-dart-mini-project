class CheckWinner {
  calPoint(String c1, c2, c3) {
    int point = 0;
    if (c1 == 'A') {
      point = point + 1;
    }
    if (c1 == '2') {
      point = point + 2;
    }
    if (c1 == '3') {
      point = point + 3;
    }
    if (c1 == '4') {
      point = point + 4;
    }
    if (c1 == '5') {
      point = point + 5;
    }
    if (c1 == '6') {
      point = point + 6;
    }
    if (c1 == '7') {
      point = point + 7;
    }
    if (c1 == '8') {
      point = point + 8;
    }
    if (c1 == '9') {
      point = point + 9;
    }
    if (c1 == 'J') {
      point = point + 10;
    }
    if (c1 == 'Q') {
      point = point + 10;
    }
    if (c1 == 'K') {
      point = point + 10;
    }
    if (c2 == 'A') {
      point = point + 1;
    }
    if (c2 == '2') {
      point = point + 2;
    }
    if (c2 == '3') {
      point = point + 3;
    }
    if (c2 == '4') {
      point = point + 4;
    }
    if (c2 == '5') {
      point = point + 5;
    }
    if (c2 == '6') {
      point = point + 6;
    }
    if (c2 == '7') {
      point = point + 7;
    }
    if (c2 == '8') {
      point = point + 8;
    }
    if (c2 == '9') {
      point = point + 9;
    }
    if (c2 == 'J') {
      point = point + 10;
    }
    if (c2 == 'Q') {
      point = point + 10;
    }
    if (c2 == 'K') {
      point = point + 10;
    }
    if (c3 == 'A') {
      point = point + 1;
    }
    if (c3 == '2') {
      point = point + 2;
    }
    if (c3 == '3') {
      point = point + 3;
    }
    if (c3 == '4') {
      point = point + 4;
    }
    if (c3 == '5') {
      point = point + 5;
    }
    if (c3 == '6') {
      point = point + 6;
    }
    if (c3 == '7') {
      point = point + 7;
    }
    if (c3 == '8') {
      point = point + 8;
    }
    if (c3 == '9') {
      point = point + 9;
    }
    if (c3 == 'J') {
      point = point + 10;
    }
    if (c3 == 'Q') {
      point = point + 10;
    }
    if (c3 == 'K') {
      point = point + 10;
    }
    return point;
  }

  void CheckWin(int ComPoint, int PlayerPoint) {
    if (ComPoint > 21 && PlayerPoint <= 21) {
      print('***************');
      print('*             *');
      print('* You Win !!! *');
      print('*             *');
      print('***************');
    } else if (ComPoint <= 21 && PlayerPoint > 21) {
      print('****************');
      print('*              *');
      print('* You Loss !!! *');
      print('*              *');
      print('****************');
    } else if (ComPoint > 21 && PlayerPoint > 21) {
      print('************');
      print('*          *');
      print('* Draw !!! *');
      print('*          *');
      print('************');
    } else if (ComPoint > PlayerPoint) {
      print('****************');
      print('*              *');
      print('* You Loss !!! *');
      print('*              *');
      print('****************');
    } else if (ComPoint < PlayerPoint) {
      print('***************');
      print('*             *');
      print('* You Win !!! *');
      print('*             *');
      print('***************');
    } else {
      print('************');
      print('*          *');
      print('* Draw !!! *');
      print('*          *');
      print('************');
    }
  }
}
