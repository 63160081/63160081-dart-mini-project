class Deck {
  List<String> deck = [
    'A',
    'A',
    'A',
    '2',
    '2',
    '2',
    '3',
    '3',
    '3',
    '4',
    '4',
    '4',
    '5',
    '5',
    '5',
    '6',
    '6',
    '6',
    '7',
    '7',
    '7',
    '8',
    '8',
    '8',
    '9',
    '9',
    '9',
    'J',
    'J',
    'J',
    'Q',
    'Q',
    'Q',
    'K',
    'K',
    'K'
  ];
  String card = '';

  getDeck() {
    return deck;
  }

  draw(deck) {
    card = deck.elementAt(0);
    deck.removeAt(0);
    return card;
  }

  shuffle() {
    deck.shuffle();
  }
}
