import 'dart:ffi';
import 'dart:io';

import 'CheckWinner.dart';
import 'Deck.dart';
import 'Com.dart';
import 'Player.dart';

class Process {
  void showWelcome() {
    print('************************************');
    print('*                                  *');
    print('* Welcome to BlackJack 21 Game !!! *');
    print('*                                  *');
    print('************************************');
    print('');
  }

  void showPlayerTurn() {
    print('Player Turn:');
    print('Do you want to draw? y/n');
  }

  void showComTurn() {
    print('Com Turn:');
  }
}

void main(List<String> args) {
  Deck gameDeck = Deck();
  List<String> deck = gameDeck.getDeck();
  Player player = Player();
  Com com = Com();
  String input = '';

  deck.shuffle();

  Process process = Process();
  CheckWinner checkWinner = CheckWinner();

  player.setPlayerCard1(gameDeck.draw(deck));
  player.setPlayerCard2(gameDeck.draw(deck));
  com.setComCard1(gameDeck.draw(deck));
  com.setComCard2(gameDeck.draw(deck));

  //Show Com Card
  process.showWelcome();
  sleep(new Duration(seconds: 2));
  print('Com card is:');
  sleep(new Duration(seconds: 1));
  print('***');
  print(com.getComCard2().toString());
  print('Com Point is : ');
  sleep(new Duration(seconds: 1));
  com.setComPoint(
      checkWinner.calPoint('***', com.getComCard2(), com.getComCard3()));
  print(com.getComPoint());
  print('');
  com.setComPoint(checkWinner.calPoint(
      com.getComCard1(), com.getComCard2(), com.getComCard3()));

  // Show Player Card
  print('your Card is:');
  sleep(new Duration(seconds: 1));
  print(player.getPlayerCard1().toString());
  print(player.getPlayerCard2().toString());
  player.setPlayerPoint(checkWinner.calPoint(player.getPlayerCard1(),
      player.getPlayerCard2(), player.getPlayerCard3()));
  print('Your Point is : ');
  sleep(new Duration(seconds: 1));
  print(player.getPlayerPoint());
  print('');

  //Player Turn
  process.showPlayerTurn();
  input = stdin.readLineSync()!;
  print(input);
  if (input == 'y') {
    print('Your draw a card !!!');
    player.setPlayerCard3(gameDeck.draw(deck));
    player.resetPlayerpoint();
    player.setPlayerPoint(checkWinner.calPoint(player.getPlayerCard1(),
        player.getPlayerCard2(), player.getPlayerCard3()));
    print('Your card is :');
    sleep(new Duration(seconds: 2));
    print(player.getPlayerCard1().toString());
    print(player.getPlayerCard2().toString());
    print(player.getPlayerCard3().toString());
    print('');
    sleep(new Duration(seconds: 1));
    print('Your Point is : ');
    sleep(new Duration(seconds: 1));
    print(player.getPlayerPoint());
    print('');
  } else {
    print('Your decision is STAY!!!');
    print('');
    print('Your Point is : ');
    sleep(new Duration(seconds: 1));
    print(player.getPlayerPoint());
    print('');
  }

  sleep(new Duration(seconds: 1));

  // Com Turn
  process.showComTurn();
  sleep(new Duration(seconds: 1));
  print('Com decision is ...');
  sleep(new Duration(seconds: 1));
  if (com.Comdecision(com.getComPoint())) {
    com.setComCard3(gameDeck.draw(deck));
    print('Com card is Draw!!!');
    print('Com Card is : ');
    sleep(new Duration(seconds: 1));
    print('***');
    print(com.getComCard2().toString());
    print(com.getComCard3().toString());
    sleep(new Duration(seconds: 1));
    print('Com Point is : ');
    sleep(new Duration(seconds: 1));
    com.resetComPoint();
    com.setComPoint(
        checkWinner.calPoint("***", com.getComCard2(), com.getComCard3()));
    print(com.getComPoint());
    print('');
    com.resetComPoint();
    com.setComPoint(checkWinner.calPoint(
        com.getComCard1(), com.getComCard2(), com.getComCard3()));
  } else {
    print('Com decision is STAY!!!');
    com.resetComPoint();
    com.setComPoint(checkWinner.calPoint(
        com.getComCard1(), com.getComCard2(), com.getComCard3()));
    print('');
  }

  sleep(new Duration(seconds: 1));
  print('The winner is ...');
  sleep(new Duration(seconds: 1));
  checkWinner.CheckWin(com.getComPoint(), player.getPlayerPoint());
  print('Your card is : ' +
      player.getPlayerCard1().toString() +
      ' ' +
      player.getPlayerCard2().toString() +
      ' ' +
      player.getPlayerCard3().toString() +
      ' ');
  print('Your Point is:' + player.getPlayerPoint().toString());
  print('');
  print('Com Card is : ' +
      com.getComCard1().toString() +
      ' ' +
      com.getComCard2().toString() +
      ' ' +
      com.getComCard3().toString() +
      ' ');
  print('Com Point is:' + com.getComPoint().toString());
  sleep(new Duration(seconds: 30));
}
