import 'dart:math';

class Com {
  String Com_card1 = '';
  String Com_card2 = '';
  String Com_card3 = '';
  int Com_point = 0;

  getComCard1() {
    return Com_card1;
  }

  getComCard2() {
    return Com_card2;
  }

  getComCard3() {
    return Com_card3;
  }

  setComCard1(String card) {
    return this.Com_card1 = card;
  }

  setComCard2(String card) {
    return this.Com_card2 = card;
  }

  setComCard3(String card) {
    return this.Com_card3 = card;
  }

  setComPoint(int point) {
    return Com_point = Com_point + point;
  }

  resetComPoint() {
    return Com_point = 0;
  }

  getComPoint() {
    return Com_point;
  }

  Comdecision(int point) {
    if (point <= 11) {
      return true;
    } else if (point > 11 && point < 20) {
      List<bool> dec = [true, false];
      dec.shuffle();
      Random random = Random();
      return dec[random.nextInt(dec.length)];
    } else if (point > 15 && point < 20) {
      List<bool> dec = [true, false, false];
      dec.shuffle();
      Random random = Random();
      return dec[random.nextInt(dec.length)];
    } else if (point > 20 && point <= 21) {
      List<bool> dec = [true, false, false, false, false];
      dec.shuffle();
      Random random = Random();
      return dec[random.nextInt(dec.length)];
    } else {
      return false;
    }
  }
}
