class Player {
  String Player_card1 = '';
  String Player_card2 = '';
  String Player_card3 = '';
  int Player_point = 0;
  getPlayerCard1() {
    return Player_card1;
  }

  getPlayerCard2() {
    return Player_card2;
  }

  getPlayerCard3() {
    return Player_card3;
  }

  setPlayerCard1(String card) {
    return this.Player_card1 = card;
  }

  setPlayerCard2(String card) {
    return this.Player_card2 = card;
  }

  setPlayerCard3(String card) {
    return this.Player_card3 = card;
  }

  setPlayerPoint(int point) {
    return Player_point = Player_point + point;
  }

  resetPlayerpoint() {
    return Player_point = 0;
  }

  getPlayerPoint() {
    return Player_point;
  }
}
